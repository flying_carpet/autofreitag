+++
title = 'AutoFreiTag'
description = 'Mach deine Schule autofrei!'
featured_image=''
draft = false
+++

## Was ist ein AutoFreiTag?

Wenn Du aktuell zur Schule gehst, kennst du wahrscheinlich das Problem: vor der Schule stapeln sich morgens und mittags förmlich die Autos. Und Du musst aufpassen, dass Du nicht von einer Person mit Auto angefahren wirst, die besorgt ihr Kind lieber in die Schule fährt, weil der Schulweg gefährlich ist.

Aus dieser Misere gibt es aber einen Ausweg - wenn alle mitmachen und ohne Auto zur Schule kommen würden, wäre der Stau, Lärm, Gestank und die Gefahr von einem auf den anderen Tag verschwunden.  
Aber wie könnte das gehen?  
Es könnten sich Schüler\*innen, Lehrer\*innen, Eltern und Schulleitung gemeinsam bei Eurer Stadtverwaltung dafür einsetzen, Regeln für den Autoverkehr zu erarbeiten, um das Aufkommen stark zu reduzieren.

Wenn Du aber nicht (nur) diesen jahrelangen Weg gehen möchtest, gibt es noch eine zweite Möglichkeit, die nur wenige Minuten bis Stunden Aufwand bedeutet und mit wenigen Tagen Vorlauf eine temporäre, autofreie Utopie ermöglicht. Haben wir dein Interesse geweckt?
